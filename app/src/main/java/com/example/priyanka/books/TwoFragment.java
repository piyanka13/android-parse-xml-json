package com.example.priyanka.books;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Priyanka on 9/22/2018.
 */

public class TwoFragment extends Fragment {

    private TextView description;
    public TwoFragment(){}

    @Override
    public void onCreate(Bundle savedInstancesState)
    {
        super.onCreate(savedInstancesState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstances)
    {
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        Bundle args = getArguments();
        ((TextView) view.findViewById(R.id.descriptionview)).setText(Html.fromHtml(args.getString("Description")));
        return view;
    }
}
