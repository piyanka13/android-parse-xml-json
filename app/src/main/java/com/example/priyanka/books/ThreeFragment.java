package com.example.priyanka.books;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Priyanka on 9/22/2018.
 */

public class ThreeFragment extends Fragment {

    public ThreeFragment(){}

    @Override
    public void onCreate(Bundle savedInstancesState)
    {
        super.onCreate(savedInstancesState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstances)
    {
        return inflater.inflate(R.layout.fragment_three, container, false);
    }
}
