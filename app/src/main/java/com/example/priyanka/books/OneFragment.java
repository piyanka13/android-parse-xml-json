package com.example.priyanka.books;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Priyanka on 9/22/2018.
 */

public class OneFragment extends Fragment {

    private TextView title;

    public OneFragment(){}

    @Override
    public void onCreate(Bundle savedInstancesState)
    {
        super.onCreate(savedInstancesState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstances)
    {
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        Bundle args = getArguments();
        ((TextView) view.findViewById(R.id.titleview)).setText(args.getString("Title"));
//
//       Log.i("info",a);
        return view;
    }

}
