package com.example.priyanka.books;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Priyanka on 9/8/2018.
 */

class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyHolderView> {

    ArrayList<HashMap<String, String>> response;
    Context context;
    private final DataAdapterOnClickHandler mClickHandler;

    public interface DataAdapterOnClickHandler{
        void onClick(  HashMap<String, String> BookData);
    }

    public DataAdapter(DataAdapterOnClickHandler clickHandler){
        mClickHandler = clickHandler;
    }

    public class MyHolderView extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextTittle, mAuthor;
        public ImageView mImageView;

        public MyHolderView(View view) {
            super(view);
            mTextTittle = (TextView) view.findViewById(R.id.textView5);
           // mAuthor = (TextView) view.findViewById(R.id.textView);
            mImageView = (ImageView) view.findViewById(R.id.imageView);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            int adapterposition = getAdapterPosition();
            HashMap<String, String> BookData = response.get(adapterposition);
            mClickHandler.onClick(BookData);
        }
    }

    @Override
    public MyHolderView onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Context context =  parent.getContext();
        int layoutForList = R.layout.listbook;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View itemview = inflater.inflate(layoutForList, parent, shouldAttachToParentImmediately);
        return new MyHolderView(itemview);
    }

    @Override
    public void onBindViewHolder(MyHolderView holder, int position){
        Context mcontext = holder.mImageView.getContext();
        HashMap<String,String> map = new HashMap<>();
        map = response.get(position);
        holder.mTextTittle.setText(Html.fromHtml(map.get(booklist.TITILE)));
     //   holder.mAuthor.setText(Html.fromHtml(map.get(booklist.DESCRIPTION)));
        Picasso.with(mcontext).load(map.get(booklist.SMALL_IMAGE_URL)).into(holder.mImageView);
       // Log.i("small_image_url", map.toString());
    }

    @Override
    public int getItemCount()
    {
        if(response==null) return 0;
        return  response.size();
    }

    public void setResponse(ArrayList<HashMap<String,String>> Data)
    {
        response = Data;
        notifyDataSetChanged();
    }
}
