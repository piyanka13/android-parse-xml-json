package com.example.priyanka.books;

import android.icu.util.RangeValueIterator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by Priyanka on 9/8/2018.
 */

final class XmlParse {

    public static  String getValue(Element item, String str)
    {
        NodeList nodeList = item.getElementsByTagName(str);
        return getElementValue(nodeList.item(0));
    }

    public static final String getElementValue(Node elem)
    {
        Node child;
        if(elem != null)
        {
            if(elem.hasChildNodes())
            {
                for(child = elem.getFirstChild(); child!=null; child= child.getNextSibling())
                {
                    if(child.getNodeType() == Node.TEXT_NODE)
                        return child.getNodeValue();
                }
            }
        }
        return "";
    }
}
