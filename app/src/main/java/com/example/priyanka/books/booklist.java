package com.example.priyanka.books;

import android.content.Context;
import android.content.Intent;
import android.icu.util.RangeValueIterator;
import android.net.NetworkRequest;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

public class booklist extends AppCompatActivity implements DataAdapter.DataAdapterOnClickHandler {

    static final String ID ="id";
    static final String SMALL_IMAGE_URL ="image_url";
    static final String TITILE ="title_without_series";
    static final String DESCRIPTION = "description";
    static final String ISBN = "isbn";
    static final String ISBN13 = "isbn13";
    static final String NUM_PAGES = "num_pages";
    static final String FORMAT = "format";
    static final String PUBLISHER = "publisher";
    static final String PUBLICATION_DAY = "publication_day";
    static final String PUBLICATION_MONTH = "publication_month";
    static final String PUBLICATION_YEAR = "publication_year";
    static final String AVERAGE_RATING = "average_rating";
    static final String NAME = "name";


    private RecyclerView MrecyclerView;
    private DataAdapter dataAdapter;
    String imagelink, title, descripption, isbn, isbn13, num_pages, format, publisher, publication_day, publication_month, publication_year, average_rating, name;
    String URL ="https://www.goodreads.com/author/show/18552?format=xml&key=NZOAsku1IoBvT97kb4YCWg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booklist);
        MrecyclerView = (RecyclerView) findViewById(R.id.recylerview);
        RecyclerView.LayoutManager layoutManager =  new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        MrecyclerView.setLayoutManager(layoutManager);
        MrecyclerView.setHasFixedSize(true);
        dataAdapter = new DataAdapter(this);
        MrecyclerView.setAdapter(dataAdapter);
        NetworkRequest task =  new NetworkRequest();
        task.execute();
    }

    @Override
    public void onClick(HashMap<String, String> BookData) {
        Context context = this;
        Class destinastion = DetailsBoook.class;
        title = BookData.get(TITILE).toString();
        imagelink = BookData.get(SMALL_IMAGE_URL).toString();
        descripption = BookData.get(DESCRIPTION).toString();
        Intent intent = new Intent(context, destinastion);
        Bundle extras = new Bundle();
        extras.putString("Title", title);
        extras.putString("Description", descripption);
        extras.putString("Image",imagelink);
        intent.putExtras(extras);
        startActivity(intent);
      //  Toast.makeText(context,BookData.get(TITILE), Toast.LENGTH_LONG).show();
    }

    public class NetworkRequest extends AsyncTask<Object, Object, ArrayList<HashMap<String,String>>>
    {
        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(Object... params) {
            String url = NetworkConnection.NetworkConnect(URL);
            try{
                Document XML = NetworkConnection.GetDomElement(url);
                ArrayList<HashMap<String, String>> JsonParser;
                JsonParser =  new ArrayList<>();
                NodeList nodeList = XML.getElementsByTagName("book");
                for(int i= 0; i<nodeList.getLength();i++)
                {
                    HashMap<String,String> map = new HashMap<>();
                    Element element =  (Element) nodeList.item(i);
                    map.put(ID,XmlParse.getValue(element,ID));
                    map.put(SMALL_IMAGE_URL, XmlParse.getValue(element, SMALL_IMAGE_URL));
                    map.put(TITILE, XmlParse.getValue(element, TITILE));
                    map.put(DESCRIPTION, XmlParse.getValue(element,DESCRIPTION));
                    map.put(ISBN,XmlParse.getValue(element, ISBN));
                    map.put(ISBN13,XmlParse.getValue(element, ISBN13));
                    map.put(NUM_PAGES,XmlParse.getValue(element, NUM_PAGES));
                    map.put(FORMAT,XmlParse.getValue(element,FORMAT));
                    JsonParser.add(map);
                }
                return JsonParser;
            }catch (SAXException e)
            {
                e.printStackTrace();
                return null;
            }catch (ParserConfigurationException e)
            {
                e.printStackTrace();
                return  null;
            }catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> Data) {
            super.onPostExecute(Data);
            if(Data !=null)
            {
                dataAdapter.setResponse(Data);
            }
        }
    }
}
