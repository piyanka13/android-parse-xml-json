package com.example.priyanka.books;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailsBoook extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView imageview;
    public String image, title, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_boook);

        Context context = this;
      //  Intent intent = getIntent();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        image = extras.getString("Image");
        imageview = (ImageView) findViewById(R.id.imageview1);
        Picasso.with(context).load(image).into(imageview);
    }

 class Getbookdetail {

     Intent intent = getIntent();
     Bundle extras = intent.getExtras();

     public String TitleBook() {
        return  title = extras.getString("Title");
     }

     public String DescritionBook()
     {
         return description = extras.getString("Description");
     }

 }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter =  new ViewPagerAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        Getbookdetail book = new Getbookdetail();
        String a = book.TitleBook();
        String b = book.DescritionBook();
        bundle.putString("Title",a);
        bundle.putString("Description",b);

        OneFragment onefragment = new OneFragment();
        TwoFragment twofragment = new TwoFragment();
        onefragment.setArguments(bundle);
        twofragment.setArguments(bundle);

        adapter.addFragment(onefragment,"Title");
        adapter.addFragment(twofragment,"Description");
        adapter.addFragment(new ThreeFragment(),"Author");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter{
        private final List<Fragment> mFragmenlist = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmenlist.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmenlist.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmenlist.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }
}
